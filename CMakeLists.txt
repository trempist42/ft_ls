cmake_minimum_required(VERSION 3.6)   # CMake version check
project(ft_ls)               # Create project "simple_example"
set(CMAKE_C_STANDARD  99)            # Enable c99 standard

file(GLOB source_files
        "libft/includes/*.h"
        "includes/*.h"
        "src/*.c"
        "libft/src/*.c"
        )

add_executable(ft_ls ${source_files}) # Add executable target with source files listed in SOURCE_FILES variable