# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rafalmer <rafalmer@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/12/29 21:21:11 by rafalmer          #+#    #+#              #
#    Updated: 2019/01/18 18:19:28 by rafalmer         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME	= libftprintf.a

SRC		= ft_printf.c

OBJ		= $(addprefix $(OBJDIR),$(SRC:.c=.o))

# compiler
CC		= gcc
CFLAGS	= -Wall -Wextra -Werror
LDFLAGS	= -L $(FT_PTH) -l ft

# ft library
FT_PTH	= ./libft/
FT_LIB	= libft.a
FT_INC	= -I $(addprefix $(FT_PTH),includes/)

# directories
SRCDIR	= ./src/
INCDIR	= ./includes/
OBJDIR	= ./obj/

.PHONY: all clean fclean re

all: $(NAME)

$(NAME): obj $(OBJ)
	@$(MAKE) -C $(FT_PTH)
	ar rc $(NAME) $(OBJ)
	ranlib $(NAME)

obj:
	@mkdir -p $(OBJDIR)

$(OBJDIR)%.o:$(SRCDIR)%.c
	$(CC) $(CFLAGS) $(FT_INC) -I $(INCDIR) -o $@ -c $<

clean:
	rm -rf $(OBJDIR)
	@$(MAKE) -C $(FT_PTH) clean

fclean: clean
	rm -rf $(NAME)
	@$(MAKE) -C $(FT_PTH) fclean

re: fclean all
