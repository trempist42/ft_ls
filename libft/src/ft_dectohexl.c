/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dectohexl.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rafalmer <rafalmer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/20 17:59:05 by rafalmer          #+#    #+#             */
/*   Updated: 2019/01/22 18:55:57 by rafalmer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

static int			ft_hexlen(unsigned int n)
{
	int		len;

	len = 0;
	while (n != 0)
	{
		n /= 16;
		len++;
	}
	return (len);
}

char	*ft_dectohexl(unsigned int n)
{
	char	*res;
	int		len;
	int		i;

	len = ft_hexlen(n);
	i = len - 1;
	if (!(res = (char *)malloc(sizeof(char) * (size_t)len + 1)))
		return (NULL);
	*(res + len) = '\0';
	while (i >= 0)
	{
		*(res + i) = ((n % 16) < 10) ? (n % 16 + '0') : (((n % 16) - 10) + 'a');
		i--;
		n /= 16;
	}
	return (res);
}
