/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dectooct.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rafalmer <rafalmer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/20 17:59:05 by rafalmer          #+#    #+#             */
/*   Updated: 2019/01/22 18:56:51 by rafalmer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

static int			ft_octlen(unsigned int n)
{
	int		len;

	len = 0;
	while (n != 0)
	{
		n /= 8;
		len++;
	}
	return (len);
}

char	*ft_dectooct(unsigned int n)
{
	char	*res;
	int		len;
	int		i;

	len = ft_octlen(n);
	i = len - 1;
	if (!(res = (char *)malloc(sizeof(char) * (size_t)len + 1)))
		return (NULL);
	*(res + len) = '\0';
	while (i >= 0)
	{
		*(res + i) = (n % 8 + '0');
		i--;
		n /= 8;
	}
	return (res);
}
