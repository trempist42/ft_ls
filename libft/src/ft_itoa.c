/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rafalmer <rafalmer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 14:08:33 by rafalmer          #+#    #+#             */
/*   Updated: 2019/01/20 17:38:32 by rafalmer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

static long int		ft_abs(long int n)
{
	if (n < 0)
		return (-n);
	else
		return (n);
}

static int			ft_intlen(long int n)
{
	int		len;

	if (n <= 0)
		len = 1;
	else
		len = 0;
	while (n != 0)
	{
		n /= 10;
		len++;
	}
	return (len);
}

char				*ft_itoa(int n)
{
	int			len;
	int			sign;
	char		*res;

	if (n < 0)
		sign = -1;
	else
		sign = 1;
	len = ft_intlen(n);
	if (!(res = (char *)malloc(sizeof(char) * (size_t)len + 1)))
		return (NULL);
	res[len] = '\0';
	len--;
	while (len >= 0)
	{
		res[len] = '0' + (char)ft_abs(n % 10);
		n = (int)ft_abs(n / 10);
		len--;
	}
	if (sign == -1)
		res[0] = '-';
	return (res);
}
