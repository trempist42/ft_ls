/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rafalmer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 16:19:06 by rafalmer          #+#    #+#             */
/*   Updated: 2018/11/25 16:21:06 by rafalmer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list		*res;
	t_list		*tmp;

	if (lst && f)
	{
		if (!(tmp = (*f)(lst)))
			return (NULL);
		res = tmp;
		while (lst->next)
		{
			lst = lst->next;
			if (!(tmp->next = (*f)(lst)))
				return (NULL);
			tmp = tmp->next;
		}
		return (res);
	}
	return (NULL);
}
