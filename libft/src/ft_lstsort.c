/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstsort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rafalmer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/18 03:34:48 by rafalmer          #+#    #+#             */
/*   Updated: 2019/03/18 05:56:16 by rafalmer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

t_list	*ft_lstsort(t_list *lst, int (*is_sorted)(void *, void *))
{
	t_list	*tmplst;
	void 	*tmp;
	int 	flag;
	int 	size;
	int 	i;
	int 	j;

	size = 0;
	i = 0;
	tmplst = lst;
	while (tmplst->next)
	{
		size++;
		tmplst = tmplst->next;
	}
	while (i < size - 1)
	{
		j = 0;
		tmplst = lst;
		flag = 0;
		while (j < size - i - 1)
		{
			if (!is_sorted(tmplst->content, tmplst->next->content))
			{
				tmp = tmplst->content;
				tmplst->content = tmplst->next->content;
				tmplst->next->content = tmp;
				flag = 1;
			}
			tmplst = tmplst->next;
			j++;
		}
		if (!flag)
			return (lst);
		i++;
	}
	return (lst);
}