/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rafalmer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/24 14:40:13 by rafalmer          #+#    #+#             */
/*   Updated: 2018/12/18 21:30:03 by rafalmer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	char			*dstptr;
	char			*srcptr;

	dstptr = (char *)dst;
	srcptr = (char *)src;
	if (srcptr > dstptr)
		while ((size_t)(srcptr - (char *)src) < len)
			*dstptr++ = *srcptr++;
	else
		while (len-- > 0)
			*(dstptr + len) = *(srcptr + len);
	return (dst);
}
