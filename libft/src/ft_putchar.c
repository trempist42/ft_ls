/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putchar.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rafalmer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/24 14:44:16 by rafalmer          #+#    #+#             */
/*   Updated: 2018/12/19 20:53:18 by rafalmer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

void	ft_putchar(char c)
{
	unsigned char d[2];

	if ((unsigned char)c < 128)
		write(1, &c, 1);
	else
	{
		d[0] = 128 + 64 + ((unsigned char)c) / 64;
		d[1] = 128 + ((unsigned char)c % 64);
		write(1, d, 2);
	}
}
