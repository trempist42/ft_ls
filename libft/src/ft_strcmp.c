/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rafalmer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 14:28:02 by rafalmer          #+#    #+#             */
/*   Updated: 2018/11/25 14:42:48 by rafalmer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

int		ft_strcmp(const void *s1, const void *s2)
{
	size_t			i;
	unsigned char	c1;
	unsigned char	c2;
	char			*p1;
	char			*p2;

	i = 0;
	p1 = (char *)s1;
	p2 = (char *)s2;
	while (p1[i] || p2[i])
	{
		c1 = (unsigned char)p1[i];
		c2 = (unsigned char)p2[i];
		if (c1 != c2)
			return (c1 - c2);
		i++;
	}
	return (0);
}
