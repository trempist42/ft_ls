/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rafalmer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/24 14:42:47 by rafalmer          #+#    #+#             */
/*   Updated: 2018/12/18 20:56:33 by rafalmer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strdup(const char *s1)
{
	char	*res;
	char	*tmp;

	if (!(res = (char *)malloc(sizeof(char) * ft_strlen(s1) + 1)))
		return (NULL);
	tmp = res;
	while (*s1)
		*tmp++ = *s1++;
	*tmp = '\0';
	return (res);
}
