/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rafalmer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/24 14:45:15 by rafalmer          #+#    #+#             */
/*   Updated: 2018/12/18 20:55:19 by rafalmer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	char		*res;
	char		*tmp;

	if (s1 && s2 && (res = ft_strnew(ft_strlen(s1) + ft_strlen(s2) + 1)))
	{
		tmp = res;
		while (*s1)
			*tmp++ = *s1++;
		while (*s2)
			*tmp++ = *s2++;
		*tmp = '\0';
		return (res);
	}
	else
		return (NULL);
}
