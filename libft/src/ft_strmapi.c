/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rafalmer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 17:52:52 by rafalmer          #+#    #+#             */
/*   Updated: 2018/11/25 17:57:44 by rafalmer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	int		i;
	char	*res;

	if (s && (res = ft_strnew(ft_strlen(s))))
	{
		i = 0;
		while (s[i])
		{
			res[i] = (*f)((unsigned int)i, s[i]);
			i++;
		}
		return (res);
	}
	else
		return (NULL);
}
