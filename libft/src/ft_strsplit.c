/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rafalmer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/24 14:46:12 by rafalmer          #+#    #+#             */
/*   Updated: 2018/11/24 14:51:41 by rafalmer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

static size_t	ft_countstr(char const *s, char c)
{
	size_t	i;
	size_t	countstr;
	size_t	new_word;

	if (!s)
		return (0);
	i = 0;
	countstr = 0;
	new_word = 1;
	while (s[i])
	{
		if (new_word && s[i] != c)
		{
			countstr++;
			new_word = 0;
		}
		if (s[i] == c)
			new_word = 1;
		i++;
	}
	return (countstr);
}

static char		*ft_setstr(char const *s, char c, size_t *start)
{
	size_t	end;

	if (!s)
		return (NULL);
	while (s[*start] == c)
		*start += 1;
	end = *start;
	while (s[*start] && s[*start] != c)
		*start += 1;
	return (ft_strsub(s, (unsigned int)end, *start - end));
}

char			**ft_strsplit(char const *s, char c)
{
	char	**res;
	size_t	countstr;
	size_t	i;
	size_t	start;

	countstr = ft_countstr(s, c);
	if (!s || !(res = (char **)malloc(sizeof(char *) * (countstr + 1))))
		return (NULL);
	i = 0;
	start = 0;
	while (countstr--)
		res[i++] = ft_setstr(s, c, &start);
	res[i] = NULL;
	return (res);
}
