/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rafalmer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/27 20:15:45 by rafalmer          #+#    #+#             */
/*   Updated: 2018/12/16 18:43:57 by rafalmer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strstr(const char *haystack, const char *needle)
{
	size_t	i;
	size_t	i1;
	size_t	pause;

	i = 0;
	if (ft_strlen(haystack) < ft_strlen(needle))
		return (NULL);
	while (haystack[i++])
	{
		pause = i - 1;
		i1 = 0;
		while (haystack[i - 1] == needle[i1])
		{
			if (needle[0] && !needle[i1 + 1])
				return ((char *)haystack + pause);
			i++;
			i1++;
		}
	}
	if (!needle[0])
		return ((char *)haystack);
	else if (haystack[1])
		return (ft_strstr(haystack + 1, needle));
	else
		return (NULL);
}
