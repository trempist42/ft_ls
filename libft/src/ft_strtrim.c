/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rafalmer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/24 14:46:12 by rafalmer          #+#    #+#             */
/*   Updated: 2018/11/24 14:51:41 by rafalmer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strtrim(char const *s)
{
	char	*res;
	size_t	i;
	size_t	i1;
	size_t	tmp;

	i = 0;
	i1 = 0;
	if (!s)
		return (NULL);
	if (!ft_strcmp(s, ""))
		return ((char *)s);
	while (s[i] == ' ' || s[i] == '\n' || s[i] == '\t')
		i++;
	tmp = i;
	i = ft_strlen(s) - 1;
	while ((s[i] == ' ' || s[i] == '\n' || s[i] == '\t') && i > 0)
		i--;
	if (i == 0)
		return (res = ft_strnew(0));
	if (!(res = ft_strnew(i - tmp + 1)))
		return (NULL);
	while (tmp <= i)
		res[i1++] = s[tmp++];
	return (res);
}
