/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rafalmer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/31 18:27:08 by rafalmer          #+#    #+#             */
/*   Updated: 2019/03/18 06:14:08 by rafalmer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"
#include <stdio.h>

static int ft_stringsort(void *first, void *second)
{
	return (ft_strcmp(first, second) > 0 ? 0 : 1);
}

int main(int argc, char **argv)
{
	DIR				*d;
	struct dirent	*dir;
	t_list			*dirlst;
	t_list			*tmplst;

	if (argc == 1)
	{
		dirlst = ft_lstnew(NULL, 0);
		if (!dirlst || !(d = opendir(".")))
		{
			perror(argv[0]);
			return (errno);
		}
		while ((dir = readdir(d)))
		{
			if ((dir->d_name)[0] != '.')
			{
				ft_lstadd(&dirlst,
						ft_lstnew(dir->d_name, ft_strlen(dir->d_name)));

			}
		}
		closedir(d);
		ft_lstsort(dirlst, ft_stringsort);
		tmplst = dirlst;
		while (tmplst->content)
		{
			ft_putstr(tmplst->content);
			ft_putchar('\n');
			tmplst = tmplst->next;
		}
	}
	else if (argc == 2)
	{
		dirlst = ft_lstnew(NULL, 0);
		if (!dirlst || !(d = opendir(argv[1])))
		{
			perror(argv[0]);
			return (errno);
		}
		while ((dir = readdir(d)))
		{
			if ((dir->d_name)[0] != '.')
			{
				ft_lstadd(&dirlst,
						ft_lstnew(dir->d_name, ft_strlen(dir->d_name)));
			}
		}
		closedir(d);
		ft_lstsort(dirlst, ft_stringsort);
		tmplst = dirlst;
		while (tmplst->content)
		{
			ft_putstr(tmplst->content);
			ft_putchar('\n');
			tmplst = tmplst->next;
		}
	}
	return (0);
}